// JavaScript Document
var defVertAcc = 0.01;
var defHorAcc = 0;
var defBounceEff = 0.9;
var defMoveEff = 0.999;

var shapes = [];


var UniversalShape = function(name, selector){
  this.name = name;
  this.selector = selector;
}

UniversalShape.prototype.info = function(){
  console.log("Name:", this.name, "Type:", this.type);
}

var ShapeChild = function(name){
  UniversalShape.apply(this,arguments)
  this.name = name;
  this.type = 'robot';
}

ShapeChild.prototype = UniversalShape.prototype;
ShapeChild.prototype.constructor = ShapeChild;

var shape = function (name, selector, startPosition){
  this.name = name;
  this.selector = selector;
  this.picRotation = 1;

  if (startPosition && startPosition.length) {
    this.moveTo(startPosition);
  }

  appendZones='';
  for (i=1;i<=9;i++)
  {
    appendZones += '<div class="zone zone' + i + '"></div>';
  }
  this.selector.append(appendZones);

  this.vertAcc = defVertAcc;
  this.vertSpeed = 0;
  this.nextVertMove = 0;

  this.horAcc = defHorAcc;
  this.horSpeed = 0;
  this.nextHorMove = 0;
  this.updateTerminals();
}

shape.prototype.changePicRotation = function(){
  var pR = this.picRotation;
  pR++;
  this.picRotation =(pR>4)?pR-4:pR;
};

shape.prototype.updateTerminals = function () {
  this.terminalTop = 0;
  this.terminalBottom = $(window).height() - this.selector.outerHeight();
  this.terminalRight = $(window).width() - this.selector.outerWidth();
  this.terminalLeft = 0;
};

shape.prototype.moveTo = function (position) {
  this.left = position[0];
  this.top = position[1];
  this.selector.css('top',this.top + 'px');
  this.selector.css('left',this.left + 'px');
};

shape.prototype.move = function(){
  this.calcSpeed();
  this.moveTo([this.selector.offset().left + this.nextHorMove, this.selector.offset().top + this.nextVertMove]);
};

shape.prototype.calcSpeed = function () {
  this.vertSpeed *= defMoveEff;
  this.top = this.selector.offset().top;

  if ((this.top + this.vertSpeed + this.vertAcc) <= this.terminalTop || (this.top + this.vertSpeed + this.vertAcc) >= this.terminalBottom) {
    if ((this.top + this.vertSpeed + this.vertAcc) >= this.terminalBottom)
      resetTime();
    this.vertSpeed = -defBounceEff * this.vertSpeed;
    this.nextVertMove = this.vertSpeed;
    this.changePicRotation();
    this.selector.css('background-image', 'url("sheep' + this.picRotation + '.png")');
  }
  else {
    if ((this.vertSpeed + this.vertAcc + this.top) >= this.terminalBottom) {
      this.nextVertMove = this.terminalBottom - this.top;
      this.vertSpeed += (this.nextVertMove / (this.vertSpeed + this.vertAcc)) * this.vertAcc;
    }
    else if ((this.vertSpeed + this.vertAcc + this.top) <= this.terminalTop) {
      this.nextVertMove = this.terminalTop - this.top;
      this.vertSpeed += (this.nextVertMove / (this.vertSpeed + this.vertAcc)) * this.vertAcc;
    }
    else {
      this.vertSpeed += this.vertAcc;
      this.nextVertMove = this.vertSpeed;
    }
  }
  // horizontal engine
  this.horSpeed *= defMoveEff;
  this.left = this.selector.offset().left;

  if ((this.left + this.horSpeed + this.horAcc) <= this.terminalLeft || (this.left + this.horSpeed + this.horAcc) >= this.terminalRight) {
    this.horSpeed = -defBounceEff * this.horSpeed;
    this.nextHorMove = this.horSpeed;
    this.changePicRotation();
    this.selector.css('background-image', 'url("sheep' + this.picRotation + '.png")');
  }
  else {
    if ((this.horSpeed + this.horAcc + this.left) >= this.terminalRight) {
      this.nextHorMove = this.terminalRight - this.left;
      this.horSpeed += (this.nextHorMove / (this.horSpeed + this.horAcc)) * this.horAcc;
    }
    else if ((this.horSpeed + this.horAcc + this.left) <= this.terminalLeft) {
      this.nextHorMove = this.terminalLeft - this.left;
      this.horSpeed += (this.nextHorMove / (this.horSpeed + this.horAcc)) * this.horAcc;
    }
    else {
      this.horSpeed += this.horAcc;
      this.nextHorMove = this.horSpeed;
    }
  }
};

shape.prototype.kick = function (angle) {
  angle = angle.match(/zone\d/)[0];
  switch (angle) {
    case 'zone1':
      this.vertSpeed += 2;
      this.horSpeed += 2;
      break;
    case 'zone2':
      this.vertSpeed += 2;
      this.horSpeed += 0;
      break;
    case 'zone3':
      this.vertSpeed += 2;
      this.horSpeed += -2;
      break;
    case 'zone4':
      this.vertSpeed += 0;
      this.horSpeed += 2;
      break;
    case 'zone5':
      x = 99;
      this.vertSpeed += ((Math.random() * x) - x);
      this.horSpeed += ((Math.random() * x) - x);
      //           $('body').append('<span id="bum" style="font-size:40px;position:absolute;top:' + this.selector.offset().top + 'px;left:' + this.selector.offset().left + 'px">BUUM!!!!</span>');
      //           $('#square').toggle('explode').remove();
      //           $('#bum').animate({'opacity':0},1500, function(){$(this).remove();});
      //           resetTime();
      //           square = '';
      //           newShape();
      break;
    case 'zone6':
      this.vertSpeed += 0;
      this.horSpeed += -2;
      break;
    case 'zone7':
      this.vertSpeed += -2;
      this.horSpeed += 2;
      break;
    case 'zone8':
      this.vertSpeed += -2;
      this.horSpeed += 0;
      break;
    case 'zone9':
      this.vertSpeed += -2;
      this.horSpeed += -2;
      break;
  }

};

shape.prototype.getBoundingBox = function () {
	a = this.selector[0];
	return [a.offsetLeft, a.offsetTop - a.offsetHeight, a.offsetLeft + a.offsetWidth, a.offsetTop];
}

function updateBoundaries(){
  square.updateTerminals();
}

function resetTime(){
  if (parseFloat($('#longest').text())<parseFloat($('#time').text()))
    $('#longest').text($('#time').text());
  time = 0.0;
}

function newShape(name, position){
  var id = 'id-' + Math.floor(Math.random() * 1000000000000) + 1;
  $('body').append('<div id="'+ id +'" class="shape"></div>');

  var newShape = new shape(name, $('#' + id), position);
  shapes.push(newShape);

  $('#' + id + ' div').on('mousedown', function(){
    newShape.kick($(this).attr('class'));
  })
}

function rangeOverlap(a ,b) {

	if (a[0] <= b[1] && b[0] <= a[1]){

		return true;
	}

	return false;
}

function hasDomColision(a, b) {
	aBox = a.getBoundingBox();
	bBox = b.getBoundingBox();

	if (rangeOverlap([aBox[0], aBox[2]],[bBox[0], bBox[2]]) && rangeOverlap([aBox[1], aBox[3]],[bBox[1], bBox[3]])) {

		return true;
	}

  return false;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function detectConflicts(shapes) {
  for (var i = 0; i < shapes.length; i++) {
    var shapeDom = shapes[i]; //.selector[0];
    for (var j = i + 1; j < shapes.length; j++) {
      if (i != j) {
        var testedShapeDom = shapes[j];//.selector[0];
        var hasConflict = hasDomColision(shapeDom, testedShapeDom);
        if(hasConflict) {
        	$("#collisionLog")[0].innerHTML = Math.floor(Math.random()*1000) + " | BUM!!! - " + shapes[i].name + ' TREFIL ' +  shapes[j].name;
        	//$("body").css('background-color', getRandomColor());
        }
      }
    }
  }
}

$(document).ready(function(){
  $('#vertAcc').val(defVertAcc);
  $('#horAcc').val(defHorAcc);
  resetTime();
  newShape('alfons', [0, 0]);

  setTimeout(newShape('hilde', [200, 200]), 3000);

  setTimeout(newShape('fero', [100, 500]), 3000);

  timeKeeper = setInterval(function(){
    for(var i=0; i < shapes.length; i++){
      detectConflicts(shapes);
      shapes[i].move();
    }
  },20);

  $('input').on('change', function(){
    defVertAcc = parseFloat($('input#vertAcc').val().replace(',','.'),10);
    defHorAcc = parseFloat($('input#horAcc').val().replace(',','.'),10);
    square.vertAcc = defVertAcc;
    square.horAcc = defHorAcc;

  });

  $("#sheepGenerator").on("mousedown", function(){

  	newShape(getRandomColor(), [Math.round(Math.random() * window.innerWidth), Math.round(Math.random() * window.innerHeight)]);

  });
  $(window).resize(updateBoundaries);
});